// cpp3_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using std::cout;
using std::endl;

void check_response(char response) {
	switch (response)
	{
	case 'y':
		cout << "You chose y or Y" << endl;
		break;
	case 'Y':
		cout << "You chose y or Y" << endl;
		break;
	case 'n':
		cout << "You chose n or N" << endl;
		break;
	case 'N':
		cout << "You chose n or N" << endl;
		break;
	default:
		cout << "You didn't choose a valid option" << endl;
		break;
	}
}

int main()
{
	check_response('a');
	check_response('y');
	check_response('Y');
	check_response('n');
	check_response('N');
	std::cin.get();
    return 0;
}

