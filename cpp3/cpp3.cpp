// cpp3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
using std::cout;
using std::endl;

int raise(int base, int exp) {
	int result = base;
	for (int i = 0; i < exp-1; i++) {
		result *= base;
	}
	return result;
};


int main()
{	
	cout << raise(2, 2) << endl;
	cout << raise(2, 8) << endl;
	std::cin.get();
    return 0;
}

